<?php 
global $wpdb;
/*If grain data option form is submit*/
if(isset($_POST['grain_submit']))
{
    /* Delete all settings */
    delete_option('grain_data_track_page_check');
    delete_option('grain_data_track_page_info');
    delete_option('grain_data_track_page_config');
    
    /*check which data track attributes are checked*/
    $track_page_check    = $_POST['data_track_page_check'];
    $pageviewinfo=array();
    if(isset($_POST['data_track_page_attributes'])){
    $pageviewinfo      = $_POST['data_track_page_attributes'];
    }

    /* Save all updated settings */    
    add_option('grain_data_track_page_check',$track_page_check); 
    add_option('grain_data_track_page_info',$pageviewinfo); 
}
?>