<?php 
/*function to get grain data page track info*/
function get_grain_data_page_track_info($case){
    switch ($case) {
        case "data_track_page_title":
                /*get title of current post*/
                $page_title = get_the_title();    	
                return $page_title; 
                break;
        case "data_track_page_publish":
                /*get publish date of current post*/ 
                $page_date = get_the_date( $format, $post_id );
                $isotime = date( 'c', strtotime( $page_date ) );      	
                return $isotime;
                break;
        case "data_track_page_categories":
                /*get categories of current post*/ 
                $categories = get_the_category();
                $cat_name = array();
                foreach ($categories as $key => $value) {
                  $cat_name[] = $value->name;
                }   	
                return $cat_name; 
                break;
        case "data_grain_get_page_tags":
                /*get tags current post*/  
                $post_tags = get_the_tags();
                $tag_name = array();
                foreach ($post_tags as $key => $value) {
                $tag_name[] = $value->name;
                }   	
                return $tag_name; 
                break;
        case "data_grain_get_page_author":
                /*get author id of current post*/ 
                $post_author_id = get_post_field( 'post_author', $post_id );     	
                return $post_author_id;
                break;
        case "data_grain_get_page_id":
                /*get ppost id of current post*/ 
                global $post;
                $page_id = $post->ID;    	
                return $page_id; 
                break;
        case "data_grain_get_page_posttype":
                /*get post type of current post*/ 
                $posttype = get_post_type(get_the_ID());   	
                return $posttype; 
                break;
        case "data_grain_get_loggedinstatus":
                /*get logged in status of current user */ 
                if(is_user_logged_in()) 
                {
                 $loggedInStatus = 'Registered User!';
                }
                else
                {
                 $loggedInStatus = 'Visitor User!';
                }   	
                return $loggedInStatus;    
                break;
        case "data_grain_get_userrole":
                /*get logged in user role */ 
                if( is_user_logged_in() ) {
                     $user = wp_get_current_user();
                     $role = ( array ) $user->roles;
                     }   	
                return $role[0];
                break;
        case "data_grain_get_wordpress_userid":
                /*get current user id*/ 
                $current_user = wp_get_current_user();
                $current_userid = $current_user->ID;   	
                return $current_userid;
                break;
        case "data_grain_get_email":
                /*get current user email*/ 
                $current_user = wp_get_current_user();
                $user_email = $current_user->user_email;   	
                return $user_email; 
                break;
        default:
                return "";
    }
}?>
