<?php 
/** Function to show data track page attributes on front end **/
function grain_pageview() {
    $track_information  =  array();
    $data_track_json    =  "{}";
    $track_page_check             = get_option('grain_data_track_page_check');
    $grain_data_track_page_config = get_option('grain_data_track_page_config'); 
    if($track_page_check == 'on')
    {
    $track_page_info  = get_option('grain_data_track_page_info');     
    foreach ($track_page_info as $key => $value) {
      $attribute_value=$grain_data_track_page_config[$key];
      if(isset($attribute_value) && $attribute_value['value']!==""){   
      $track_information[$attribute_value['value']] = get_grain_data_page_track_info($key);     
      }    
    }
    if(is_array($track_information) && count($track_information))
    {
           $data_track_json = json_encode($track_information);
    }
    
    /*Add Data track page attrbute in body Tag*/
    $track_attr = "<script type='text/javascript'>var custom  = '$data_track_json';
    var body = document.getElementsByTagName('body')[0];
    body.setAttribute('data-track-page', custom);</script>";
    echo $track_attr; 
 }
}
add_action('wp_footer', 'grain_pageview');
?>