<?php
/*
Plugin Name: Grain Data
Version : 1.0.0.
Author : Grain Data Consultants
Plugin URI: http://wordpress.com/
Description: The plugin that is used to add the data track page attribute to all pages.
Version: 1.0.0
Author: Grain Data Consultants
Author URI: https://graindataconsultants.com/
License: GPLv2 or later
Text Domain: Grain Data Consultants
*/

global $wpdb;
define( 'GRAIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'GRAIN_URL', plugins_url( '' ,  __FILE__ ) );


/** Include necessary wordpress files **/
require_once( dirname( dirname( __FILE__ ) ) .'/../../wp-load.php' );
require_once( dirname( dirname( __FILE__ ) ) .'/../../wp-includes/wp-db.php' );
require_once( dirname( dirname( __FILE__ ) ) .'/../../wp-includes/pluggable.php' );
require_once( dirname( dirname( __FILE__ ) ) .'/../../wp-includes/general-template.php' );
require_once( dirname( dirname( __FILE__ ) ) .'/../../wp-admin/includes/template.php' );
require_once( dirname( dirname( __FILE__ ) ) .'/../../wp-admin/includes/plugin.php' );

/** Include necessary plugin files **/
require_once('admin/grain-data-save.php');
require_once('front/grain-data-front.php');
require_once('functions/grain-data-functions.php');

/** Data Track Page Attribute Configuration Array **/
$grain_data_track_page_config = array(
"data_track_page_title"=>array("title"=>"Page Title","value"=>"pageTitle"),
"data_track_page_publish"=>array("title"=>"Page Publish Date","value"=>"pagePublishDate"),
"data_track_page_categories"=>array("title"=>"Page Categories","value"=>"pageCategories"),
"data_grain_get_page_tags"=>array("title"=>"Page Tags","value"=>"pageTags"),
"data_grain_get_page_author"=>array("title"=>"Page Author ID","value"=>"pageAuthorID"),
"data_grain_get_page_id"=>array("title"=>"Page ID","value"=>"pageID"),
"data_grain_get_page_posttype"=>array("title"=>"Page PostType","value"=>"pagePostType"),
"data_grain_get_loggedinstatus"=>array("title"=>"LoggedIn Status","value"=>"loggedInStatus"),
"data_grain_get_userrole"=>array("title"=>"UserRole","value"=>"userRole"),
"data_grain_get_wordpress_userid"=>array("title"=>"WordPress UserID","value"=>"wordPressUserID"),
"data_grain_get_email"=>array("title"=>"Email","value"=>"email"));

add_option('grain_data_track_page_config',$grain_data_track_page_config);  


/** Include css and js files **/
function grain_admin_scripts() {
    wp_enqueue_style( 'grain-data-css', GRAIN_URL . '/admin/assets/css/mystyle.css', false);
    wp_enqueue_script( 'grain-data-js', GRAIN_URL . '/admin/assets/js/myscript.js', false);
}
add_action( 'admin_enqueue_scripts', 'grain_admin_scripts' );


/** Dashboard menu action hook **/
add_action('admin_menu', 'grain_my_menu_pages');
function grain_my_menu_pages(){
    add_menu_page('Grain Data', 'Grain Data', 'manage_options', 'grain_data', 'grain_my_menu_output' );
}

/** Callback function of admin menu **/
function grain_my_menu_output(){ 
    require_once('admin/grain-data-options.php');
} 

/** Uninstall hook for plugin **/
register_uninstall_hook( __FILE__, 'grain_data_deactivation' );
function grain_data_deactivation() {
   /*Delete all the data from database if plugin is deleted*/
    delete_option('grain_data_track_page_check');
    delete_option('grain_data_track_page_info');
    delete_option('grain_data_track_page_config');
}
?>