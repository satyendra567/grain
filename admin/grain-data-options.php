 <?php 
   /* Get information from database */
    $track_page_check             = get_option('grain_data_track_page_check');
    $track_page_info              = get_option('grain_data_track_page_info');
    $grain_data_track_page_config = get_option('grain_data_track_page_config'); 
 ?>

<div class="container">
    <div class="page-header page-header-box">
     <h1>Grain Data Tracking Solutions</h1>
    </div>
    <form action="" method="post" class="attributes_bx">
    <h2>Enable Data Attributes</h2>
    <div class="form-group">
        <input type="checkbox" class="graincheck" id="text" name="data_track_page_check" <?php if($track_page_check == 'on') {?> checked <?php } ?>/>
        <label for="text"><h3>Data Track Page</h3></label>
    </div>         
    <div style= "<?php echo $track_page_check=="on"?"display:block":"display:none"?>" id="data_track_page">          
    <div class="col-md-12 attributes_box"> 
       <div class="form-group checkbox2_mrgn">
         <label for="checkbox2"><h2>Please select attributes</h2></label>
       </div> 
       <?php 
       /*Loop on all the Data Track Page Attributes*/
       foreach($grain_data_track_page_config as $key => $attr_val)  {?>                              
       <div class="form-group">
        <input type="checkbox" class="filled-in" id="<?php echo $key?>" name="data_track_page_attributes[<?php echo $key;?>]" <?php echo isset($track_page_info[$key])?"checked":"" ?>/>
        <label for="<?php echo $key?>"><?php echo $attr_val[title]; ?></label>
        </div>
       <?php } ?>
    </div>              
    </div> 
    <p class="submit">
        <input type="submit" class="button-primary gos_submit" name="grain_submit" value="Save Changes">
    </p> 
    </form>
</div>